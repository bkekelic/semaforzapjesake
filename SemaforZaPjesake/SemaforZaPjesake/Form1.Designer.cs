﻿namespace SemaforZaPjesake
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pb_Scene = new System.Windows.Forms.PictureBox();
            this.btnStartScene = new System.Windows.Forms.Button();
            this.timerForAll = new System.Windows.Forms.Timer(this.components);
            this.btn_TurnONPedGreen = new System.Windows.Forms.Button();
            this.timerStopage = new System.Windows.Forms.Timer(this.components);
            this.btn_TurnONPedGreen2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Scene)).BeginInit();
            this.SuspendLayout();
            // 
            // pb_Scene
            // 
            this.pb_Scene.BackColor = System.Drawing.Color.Green;
            this.pb_Scene.Location = new System.Drawing.Point(0, 0);
            this.pb_Scene.Margin = new System.Windows.Forms.Padding(2);
            this.pb_Scene.Name = "pb_Scene";
            this.pb_Scene.Size = new System.Drawing.Size(1021, 573);
            this.pb_Scene.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Scene.TabIndex = 0;
            this.pb_Scene.TabStop = false;
            // 
            // btnStartScene
            // 
            this.btnStartScene.Location = new System.Drawing.Point(925, 0);
            this.btnStartScene.Margin = new System.Windows.Forms.Padding(2);
            this.btnStartScene.Name = "btnStartScene";
            this.btnStartScene.Size = new System.Drawing.Size(96, 48);
            this.btnStartScene.TabIndex = 1;
            this.btnStartScene.Text = "Upali semafor";
            this.btnStartScene.UseVisualStyleBackColor = true;
            this.btnStartScene.Click += new System.EventHandler(this.btnStartScene_Click);
            // 
            // timerForAll
            // 
            this.timerForAll.Interval = 1000;
            this.timerForAll.Tick += new System.EventHandler(this.timerForAll_Tick);
            // 
            // btn_TurnONPedGreen
            // 
            this.btn_TurnONPedGreen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_TurnONPedGreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TurnONPedGreen.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_TurnONPedGreen.Location = new System.Drawing.Point(305, 519);
            this.btn_TurnONPedGreen.Margin = new System.Windows.Forms.Padding(2);
            this.btn_TurnONPedGreen.Name = "btn_TurnONPedGreen";
            this.btn_TurnONPedGreen.Size = new System.Drawing.Size(23, 29);
            this.btn_TurnONPedGreen.TabIndex = 2;
            this.btn_TurnONPedGreen.UseVisualStyleBackColor = false;
            this.btn_TurnONPedGreen.Click += new System.EventHandler(this.btn_TurnONPedGreen_Click);
            // 
            // timerStopage
            // 
            this.timerStopage.Interval = 1000;
            this.timerStopage.Tick += new System.EventHandler(this.timerStopage_Tick);
            // 
            // btn_TurnONPedGreen2
            // 
            this.btn_TurnONPedGreen2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_TurnONPedGreen2.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TurnONPedGreen2.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_TurnONPedGreen2.Location = new System.Drawing.Point(707, 112);
            this.btn_TurnONPedGreen2.Margin = new System.Windows.Forms.Padding(2);
            this.btn_TurnONPedGreen2.Name = "btn_TurnONPedGreen2";
            this.btn_TurnONPedGreen2.Size = new System.Drawing.Size(23, 29);
            this.btn_TurnONPedGreen2.TabIndex = 4;
            this.btn_TurnONPedGreen2.UseVisualStyleBackColor = false;
            this.btn_TurnONPedGreen2.Click += new System.EventHandler(this.btn_TurnONPedGreen2_Click);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1022, 574);
            this.Controls.Add(this.btn_TurnONPedGreen2);
            this.Controls.Add(this.btn_TurnONPedGreen);
            this.Controls.Add(this.btnStartScene);
            this.Controls.Add(this.pb_Scene);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Semafor za pješake";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Scene)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_Scene;
        private System.Windows.Forms.Button btnStartScene;
        private System.Windows.Forms.Timer timerForAll;
        private System.Windows.Forms.Button btn_TurnONPedGreen;
        private System.Windows.Forms.Timer timerStopage;
        private System.Windows.Forms.Button btn_TurnONPedGreen2;
    }
}

